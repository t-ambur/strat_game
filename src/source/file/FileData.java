package source.file;

public class FileData {
	
	// Files holding settings and load data
	public static final String OPTIONS_PATH = "resources/settings/options.txt";
	
	// Files holding map data
	public static final String PENINSULA = "resources/worlds/peninsula.txt"; // 25x25 02August2018
	public static final String GREECE = "resources/worlds/greece.txt"; //50x50 02August2018
}
